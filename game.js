/*-------- Constants --------*/
let COLS = 6;
let ROWS = 7;

/*-------- Game State --------*/

let board = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
]
let turn = 1;
let startingTurn = 1;
let emptySlots = COLS * ROWS;

//DOM
let columns = document.querySelectorAll(".column");
let hint = document.getElementById("hint");

/*-------- Event Listeners --------*/

function addEventListeners() {
    for (let i = 0; i < COLS; i++) {
        columns[i].addEventListener("click", placeChecker);
    }
    hint.addEventListener("click", showInstructions);
}

/*-------- Logic --------*/

function checkForWinner(colNum, rowNum) {
    for (let i = 0; i < COLS; i++) {
        for (let j = 0; j < ROWS; j++) {
            //skip empty slots
            if (board[i][j] == 0) {
                continue;
            }
            //check columns
            if (j < 4 
                && board[i][j + 1] == board[i][j]
                && board[i][j + 2] == board[i][j]
                && board[i][j + 3] == board[i][j]
                ){
                    resolveGame(turn);
                    return 1;
            }
            //check rows
            if(i < 3
                && board[i + 1][j] == board[i][j]
                && board[i + 2][j] == board[i][j]
                && board[i + 3][j] == board[i][j]
                ){
                    resolveGame(turn);
                    return 1;
            }
            //check "/" diagonal starting where board[i][j] == "x"
            //  0  1  2  3  4  5
            //0[-][-][-][-][-][-]
            //1[-][-][-][-][-][-]
            //2[-][-][-][-][-][-]
            //3[x][x][x][-][-][-]
            //4[x][x][x][-][-][-]
            //5[x][x][x][-][-][-]
            //6[x][x][x][-][-][-]
            if(i < 3 && j > 2
                && board[i + 1][j - 1] == board[i][j]
                && board[i + 2][j - 2] == board[i][j]
                && board[i + 3][j - 3] == board[i][j]
                ){
                    resolveGame(turn);
                    return 1;
            }
            //check "\" diagonal starting where board[i][j] == "x"
            //  0  1  2  3  4  5
            //0[-][-][-][-][-][-]
            //1[-][-][-][-][-][-]
            //2[-][-][-][-][-][-]
            //3[-][-][-][x][x][x]
            //4[-][-][-][x][x][x]
            //5[-][-][-][x][x][x]
            //6[-][-][-][x][x][x]
            if(i >= 3 && j > 2
                && board[i - 1][j - 1] == board[i][j]
                && board[i - 2][j - 2] == board[i][j]
                && board[i - 3][j - 3] == board[i][j]
                ){
                    resolveGame(turn);
                    return 1;
            }
        }
    }
    return 0;
}

function findEmptySlot(column) {
    for (let i = 0; i < ROWS; i++) {
        if (board[column][i] == 0) {
            return i;
        }
    }
    return -1;
}

function placeChecker() {
    let colNum = this.id.substring(3, 4);
    let emptyCell = findEmptySlot(colNum);
    if (emptyCell == -1) {
        return;
    }
    else {
        let cellId = String(colNum + ',' + (COLS - emptyCell));
        document.getElementById(cellId).className = String("p" + turn + "checker");
    }
    //update game state
    updateGameState(colNum, emptyCell);
}

function resetGame(resolution) {
    //reset board
    for (let i = 0; i < COLS; i++) {
        for (let j = 0; j < ROWS; j++) {
            board[i][j] = 0;
            document.getElementById(String(i + "," + j)).className = "empty";
        }
    }
    //set starting turn
    if (resolution == 0) {
        turn = startingTurn % 2 +1;
        startingTurn = turn;
        switchTurnDisplay();
    }
    else{
        console.log(resolution);
        turn = resolution % 2 + 1;
        startingTurn = turn;
        switchTurnDisplay();
    }
    //reset emptySlots
    emptySlots = COLS * ROWS;
}

function resolveGame(resolution) {
    //0 = tie; 1 = p1 win; 2 = p2 win
    let message = "";
    switch (resolution) {
        case 0:
            message = "Its A Tie. Play Again?";
            break;
        case 1:
            message = "Player 1 Wins! Play Again?";
            break;
        case 2:
            message = "Player 2 Wins! Play Again?";
            break;
    }
    
    if (confirm(message)) {
        resetGame(resolution);
    }
    else {
        alert("Thank you for playing! Goodbye.");
        window.close();
    }
}

function showInstructions() {
    let instructions = "The rules are simple: Try to build a row of four checkers while keeping your opponent from doing the same. A row can be made horizontally, vertically, or diagonally. Click on a column to begin!";
    let rules = document.getElementById("rules");
    rules.textContent = instructions;
    rules.classList.toggle("show");
}

function switchTurnDisplay() {
    if (turn == 1) {
        document.getElementById("turnDisplay").textContent = "PLAYER 1'S TURN";
    }
    else {
        document.getElementById("turnDisplay").textContent = "PLAYER 2'S TURN";
    }

}

function updateGameState(colNum, emptyCell) {
    //update board
    board[colNum][emptyCell] = turn;
    if(checkForWinner(colNum, emptyCell) != 1){
        //switch player
        turn = turn % 2 + 1;
        switchTurnDisplay();
        //update empty slots
        emptySlots -= 1;
        if (emptySlots == 0) {
            resolveGame(0);
        }
    }
    
}

addEventListeners();